<?php
error_reporting(0); /// Disable PHP Warning to User
  
  
function AtoS($A,$split=""){return implode($split,$A);}//Array to String
function getKey($A){return array_keys($A);}//Get all key in array dict
function len($A){ if (is_array($A)){return count($A);} return strlen($A); }//Get length of array or string
function isNull($v){return is_null($v);}
function isNotNull($v){return !is_null($v);}
function replaceAll($s,$ss,$ns){return str_replace($ss,$ns,$s);}//Replace ss with ns in s
function upper($s){return strtoupper($s);}//Uppercase
function str($v){return strval($v);}

/////////////////////////////////
/////////// FILE PACK /////////// For PHP
// 
// !!! Call assign direct parameter will cause assign new value to local variable which has same name !!!
// !!! Seek then Write will OverWrite from Seek position
// uppercase function : root public folder access
// lowercase function : access from $PATH for safety
//
function addFolder($sPath="test",$sName=""){if (len($sPath)>0 && len($sName)==0){$sName=$sPath;$sPath="";} $s=len($sPath)==0?$sName:($sPath."/".$sName); if (!file_exists($s)){mkdir($s, 0777, true);}}
$PATH="DISK";
addFolder($PATH);
/// LOOK LIKE x10hosting auto encode decode UTF-8
function SAVE_FILE($sName="test.txt",$s=""){$f=fopen($sName,"w");fwrite($f,$s);fclose($f);}
function saveFile($sName="test.txt",$s=""){$f=fopen($GLOBALS['PATH']."/".$sName,"w");fwrite($f,$s);fclose($f);}
function APPEND_FILE($sName="test.txt",$s=""){$f=fopen($sName,"a");fwrite($f,$s);fclose($f);}
function appendFile($sName="test.txt",$s=""){$f=fopen($GLOBALS['PATH']."/".$sName,"a");fwrite($f,$s);fclose($f);}
function DEL_FILE($sName="test.txt"){return unlink($sName);}
function delFile($sName="test.txt"){return unlink($GLOBALS['PATH']."/".$sName);}

function OPEN_FILE($sName="test.txt"){return fopen($sName,"c+");}
function openFile($sName="test.txt"){return fopen($GLOBALS['PATH']."/".$sName,"c+");}
function SIZE_FILE($sName){$i=filesize($sName);if ($i==false){return -1;}return $i;}
function sizeFile($sName){$i=filesize($GLOBALS['PATH']."/".$sName);if ($i==false){return -1;}return $i;}
function seekFile($f,$i=0){fseek($f,$i);} /// !!! SLOW UPDATE BY OS. Return 0,-1
function WRITE_FILE($f,$s=""){return fwrite($f,$s);} /// return potition or false
function WRITE_FILE_AT($f,$s="",$i=0){ if (fseek($f,$i)==0){return fwrite($f,$s);}return false;} /// return potition or false
function SEEK_FILE($f,$i=0){return fseek($f,$i);} /// !!! SLOW UPDATE BY OS. Return 0,-1
function readS($f,$buffer=4096){return fgets($f,$buffer+1);}
function readF($f,$buffer=4096){return fread($f,$buffer);}
function readS_AT($f,$buffer=4096,$i=0){if (fseek($f,$i)==0){return fgets($f,$buffer+1);}return false;}
function readF_AT($f,$buffer=4096,$i=0){if (fseek($f,$i)==0){return fread($f,$buffer);}return false;}
function closeFile($f){if (isNotNull($f)){fclose($f);}}

///////////
/// END /// FILE PACK /// For PHP
///////////

/////////// LOG PACK ///
$GLOBALS['lSig']=SIZE_FILE("log.txt");if($GLOBALS['lSig']==-1){SAVE_FILE("log.txt","");} $GLOBALS['fSig']=OPEN_FILE("log.txt");
function SIG($s=""){SEEK_FILE($GLOBALS['fSig'],$GLOBALS['lSig']);if(WRITE_FILE($GLOBALS['fSig'],$s.chr(10))!=false){$GLOBALS['lSig']+=strlen($s)+1;}}
/// END /// LOG PACK /// For PHP

function resHeadJSON($kUTF8=false){if ($kUTF8){header('Content-Type: application/json; charset=utf-8');}else{header('Content-Type: application/json');}}
function resHeadHTML($kUTF8=false){if ($kUTF8){header('Content-Type: text/html; charset=utf-8');}else{header('Content-Type: text/html;');}}
function resHeadTEXT($kUTF8=false){if ($kUTF8){header('Content-Type: text/plain; charset=utf-8');}else{header('Content-Type: text/plain;');}}
function resBodyJSON($data){echo json_encode($data);}
function resFlush(){ob_flush();flush();}
function resEnd(){exit();}
$SSEId=0;$nSSEId=0;
function resHeadSSE(){session_start();session_write_close();// Make session read-only
    ignore_user_abort(true);// Disable default disconnect checks
    header("Content-Type: text/event-stream"); header("Cache-Control: no-cache"); header("Access-Control-Allow-Origin: *");// Set headers for stream
    $GLOBALS['SSEId']=floatval(isset($_SERVER["HTTP_LAST_EVENT_ID"]) ? $_SERVER["HTTP_LAST_EVENT_ID"] : 0);// Check if this's a new stream or an existing one?
    if ($GLOBALS['SSEId']==0){$GLOBALS['SSEId']=floatval(isset($_GET["SSEId"])?$_GET["SSEId"] : 0);}
    echo ":".str_repeat(" ",2048)."\n";echo "retry: 2000\n";//2kB padding for IE
}
function resSSE($s){if(connection_aborted()){exit();}
    else{$GLOBALS['nSSEId'] = $GLOBALS['SSEId']+1;
        if($GLOBALS['SSEId']<$GLOBALS['nSSEId']){echo "data:".$s."\n\n";$GLOBALS['SSEId'] = $GLOBALS['nSSEId'];}
    ob_flush();flush();}
}
function resSSEJSON($o){resSSE(json_encode($o));}
/// ECHO ANY THING CAN RESPONSE XMLHTTP :))
/*
startSSE();
while(true){
    resSSEJSON(array('id'=>$GLOBALS['nSSEId'],  'value1'=>'value1','value2'=>'value2'  ));
    usleep(20000);
}
*/

//// IP 
function getUserIP(){
    if(isset($_SERVER["HTTP_CF_CONNECTING_IP"])){$_SERVER["REMOTE_ADDR"]=$_SERVER["HTTP_CF_CONNECTING_IP"];$_SERVER["HTTP_CLIENT_IP"]=$_SERVER["HTTP_CF_CONNECTING_IP"];}
    $client= @$_SERVER["HTTP_CLIENT_IP"];$forward= @$_SERVER["HTTP_X_FORWARDED_FOR"];$remote= $_SERVER["REMOTE_ADDR"];
    if(filter_var($client, FILTER_VALIDATE_IP)){$ip = $client;}
    elseif(filter_var($forward, FILTER_VALIDATE_IP)){$ip = $forward;}
    else{$ip = $remote;}
    return $ip;
}// Get real visitor IP behind CloudFlare network



try {

  /// Speed priority: JSON , form, normal load
  $reqMode=0;// 0,1,2 : normal, form submit, JSON
  $reqType=$_SERVER["CONTENT_TYPE"];/// '' , 'application/x-www-form-urlencoded' , 'application/json; charset=utf-8' ,...
  $nameOrigin=$_SERVER["HTTP_ORIGIN"];
  $rawReq=file_get_contents('php://input');/// Raw request data NOT multipart/form-data
  $reqJSON=json_decode($rawReq,true);/// Json data of request
  $arrREQ=array(); $keyJSON=array(); $keyREQ=array(); if(stripos($reqType,'n/json')!=null){$reqMode=2;if(isNotNull($reqJSON)){$keyJSON=getKey($reqJSON);$keyREQ=$keyJSON;$arrREQ=$reqJSON;}}
  
  $keyPOST=getKey($_POST);/// Array all key in POST request
  $keyGET=getKey($_GET);/// Array all key in POST request
  $isReqJSON=len($keyPOST)>0||len($keyGET)>0||$reqMode==2;
  if (len($keyPOST)>0){$reqMode=1;$keyREQ=$keyPOST;$arrREQ=$_POST;}
  elseif (len($keyGET)>0){$reqMode=1;$keyREQ=$keyGET;$arrREQ=$_GET;}
  /// Easy simple access: 
  ///   reqMode : 0,1,2 : request normal loadpage , form submit GET/POST,  JSON send request
  ///   keyREQ , arrREQ : array list all key of request,  request data as array
  $HEADERS=getallheaders();/// SOLVE HttpXML Request (XHR)
  $kAUTH=false;$sAUTH="";/// Simple check Auth and get Auth key
  $kTOKEN=false;$sTOKEN="";
  $kBEARER=false;$sBEARER="";
  if (array_key_exists('Authorization', $HEADERS)){
      if (substr($HEADERS['Authorization'], 0, 6) == 'Token ') {$kTOKEN=$kAUTH=true;$sTOKEN=trim(substr($HEADERS['Authorization'], 6));$sAUTH=$sTOKEN;}
      elseif (substr($HEADERS['Authorization'], 0, 7) == 'Bearer ') {$kBEARER=$kAUTH=true;$sBEARER=trim(substr($HEADERS['Authorization'], 7));$sAUTH=$sBEARER;}
  }
  


  //saveFile("abc.txt",chr(65).chr(7).chr(66).chr(10).chr(48));
  //saveFile("abc.dat",chr(65).chr(7).chr(66).chr(10).chr(48));
  echo '<DOCTYPE html><html><head><style>
    html *{font-size: 0.95em;color:#fff !important;font-family: Courier New !important;border-radius:8px;text-shadow:0px 0px 28px #fff;outline:none;}
    @keyframes spanlink {0%{letter-spacing:0px;} 100%{letter-spacing:4px;}}
    @keyframes shrinklink {0%{letter-spacing:4px;} 100%{letter-spacing:0px;}}
    @keyframes glow{0%{text-shadow:0px 0px 28px #fff;} 50%{text-shadow:0px 0px 5px #fff,0px 0px 8px #fff,0px 0px 13px #fff,0px 0px 28px #fff;} 100%{text-shadow:0px 0px 28px #fff;}}
    .Title{text-shadow:0px 0px 28px #fff;animation:glow ease-in-out 3s infinite;}
    font{cursor:default;}
    br{display:block;content:"";margin-top:0px;height:8px;}
    .TeBox{padding:5;color:#0fe;background-color:#2436;border:1px solid #0fe;}
    .TeBox:focus{background-color:#2438;}
    .ChBox{accent-color:#0fe;width:20px;height:20px;}
    textarea{cursor:default;resize:none;font-size:12px;line-height:12px;padding:10;color:#0fe;border:1px solid #00ffee;}
    a{color:#fff;text-decoration:none;font-weight:bold;animation-name:shrinklink;animation-duration: 500ms;}
    a:hover{text-shadow:0px 0px 18px #fff,0px 0px 7px #fff;letter-spacing:4px;animation-name:spanlink;animation-duration: 500ms;}
    button{background-color:#4768;padding:3px;padding-left:8px;padding-right:8px;border:2px outset #0fe;box-shadow:0px 0px 21px #afe4;}
    button:hover{background-color:#5878;}
    button:active{background-color:#2438;font-size:1.1em;border:2px inset #0fe;}
    div{text-align:center;}
    ::-webkit-scrollbar{width:10px;height:10px;}
    ::-webkit-scrollbar-track{background:#0000;}
    ::-webkit-scrollbar-thumb{background:#0a9d;border-radius:10px;box-shadow:0px 0px 10px -3px #0fe;}
    ::-webkit-scrollbar-thumb:hover{background:#0ebf;}
    ::-webkit-scrollbar-corner {background:#0000;}
  </style></head><body style="margin:0;padding:0;background-color:#444;">';
  echo '<div style="position:fixed;width:100%;height:100%;background-size:cover;filter: blur(8px);-webkit-filter: blur(8px);"></div>';
  echo '<div style="position:fixed;width:100%;height:100%;background-color:#0004"><br>';
  //echo "<font>[<a href='note.php'> Note </a>|<a href='note.php'> Note 2 </a>|<a href='note.php'> Note 3 </a>|<a href='note.php'> Scanner </a>|<a href='note.php'> Converter </a>]</font>";
  //echo "<br><br>";
  $xfce="77x77";$Xfce=upper($xfce);
  $sf=$arrREQ['Data'];
  $sFileName=$arrREQ['sFile'];
  $sCode=$arrREQ['sCode'];
  $kRemember=$arrREQ['kRemember'];
  $kAd=0;if($arrREQ['sCode']==$xfce){$kAd=1;}elseif($arrREQ['sCode']==$Xfce){$kAd=2;}
  if ($kAd>0){
      if ($arrREQ['kMode']=="Load"){
        $zf=null;if ($kAd==2){$zf=OPEN_FILE($arrREQ['sFile']);$sf=readF($zf,$buffer=SIZE_FILE($arrREQ['sFile']));}else{$zf=openFile($arrREQ['sFile']);$sf=readF($zf,$buffer=sizeFile($arrREQ['sFile']));}
        closeFile($zf);
      }
      elseif ($arrREQ['kMode']=="Save"){
          if ($kAd==2){SAVE_FILE($arrREQ['sFile'],$arrREQ['Data']);}else{saveFile($arrREQ['sFile'],$arrREQ['Data']);}
      }
      echo '<script>setTimeout(function(){},500);</script>';
  }
  $sData=replaceAll($sf,array("&","/"),array("&#38;","&#47;"));
  echo '<form action="" method="post" id="Form1" >
    <label for="fname">File name: </label><input type="text" name="sFile" size="8" class="TeBox" spellcheck="false" autocorrect="off" autocapitalize="none" value="'.$sFileName.'">&nbsp;
    <label for="lname">Code: </label><input type="password" name="sCode" size="8" class="TeBox" value="'.(($kRemember=="1")?$sCode:'').'">
    &nbsp;<input class="ChBox" type="checkbox" name="kRemember" value="1" '.(($kRemember=="1")?'checked':'').'>&nbsp;
    <button type="submit" name="kMode" value="Load">Load</button>&nbsp;&nbsp;
    <button type="submit" name="kMode" value="Save" >Save</button><br><br>
    <textarea id="tebox1" spellcheck="false" name="Data" form="Form1" style="text-shadow:none;box-shadow:0px 0px 42px -10px #afe8;width:96%;height:88%;background-color:#0000;" wrap="off">'.$sData.'</textarea>
  </form>
';

  echo "</div></body></html>";
  
  //}
}
catch(Exception $e) {
  echo 'Message: ' .$e->getMessage();
}


?>
